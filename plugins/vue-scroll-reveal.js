// In main.js
import Vue from 'vue'
import VueScrollReveal from 'vue-scroll-reveal';

Vue.use(VueScrollReveal, {
    class: ' ', // A CSS class applied to elements with the   directive; useful for animation overrides.
    duration: 1800,
    scale: 1,
    distance: '35px',
    mobile: false
  });