// export const state = () => ({
//     counter: 0
//   })
  
//   export const mutations = {
//     increment (state) {
//       state.counter++
//     }
//   }


  
export const state = () => ({
  slides: null,
  tests: null
})

export const actions = {
  // nuxtServerInit is called by Nuxt.js before server-rendering every page
  async nuxtServerInit({ commit, dispatch }) {
    await dispatch('storeDispatchFunc')
  },
  
  // axios...
  async storeDispatchFunc({ commit }) {
    const data3  = await this.$axios.get('https://backend.molinadelrey.com/molina/items/landingpage?fields=*.*.*.*')
    const   data2  = await this.$axios.get('https://backend.molinadelrey.com/molina/items/sonstige')
    // const data2 = data.push(test)
    commit('SET_DATA', data3.data)
    commit('SET_DATA2', data2.data)

  },
}

export const mutations = {
  SET_DATA(state, theData) {
    state.slides = theData.data 
  },
  SET_DATA2(state, theData) {
    state.tests = theData.data
  },
}