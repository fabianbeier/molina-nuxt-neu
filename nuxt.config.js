// import axios from 'axios'

// let dynamicRoutes = () => {
//   return axios.get('https://backend.molinadelrey.com/molina/items/produkte?fields=*').then(res => {
//     return res.data.data.map(slug => `/products/${slug.slug}`)
//   })
//  }

export default {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  target: 'static',
  head: {
    title: 'Molina Del Rey',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/5.4.5/css/swiper.min.css' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Amiri:ital,wght@0,400;0,700;1,400;1,700&display=swap' },
      { rel: 'stylesheet', href: 'https://cdn.snipcart.com/themes/v3.0.17/default/snipcart.css' },
      { rel: 'stylesheet', href: '/swiper-overwrite.css' }
    ],
    // script: [
    //   { src: 'https://cdn.snipcart.com/themes/v3.0.17/default/snipcart.js', body: true }
    // ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/plugins/vue-animejs',
    '~/plugins/filters',
    { src: '~/plugins/vue-scroll-reveal', ssr: false },
    { src: '@/plugins/cookie-laws', ssr: false }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    // '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    '@nuxtjs/stylelint-module'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // 'nuxt-responsive-loader'
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    
  },

  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  },
  /// Router
  // router: {
  //   base: '/'
  // },
  generate: {
    interval: 500
  },
  transition:
  {
    name: 'layout',
    mode: 'out-in'
  }
}
